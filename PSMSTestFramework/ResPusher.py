import threading
import sys
import os
import json
import subprocess
import importlib
import re
import traceback
import socket
import base64
import time
import pickle
import requests

from flask import redirect
from flask import Flask
from flask import render_template
from flask import request
from flask import send_file
from flask import session


from gevent import pywsgi
from gevent.pool import Pool

import PSMSTestFramework.sysapi as sysapi
import PSMSTestFramework.views.testViews as testViews
import PSMSTestFramework.views.accountViews as accountViews
import PSMSTestFramework.views.updateViews as updateViews
import PSMSTestFramework.views.runnerViews as runnerViews



class ResPusher(threading.Thread):
    def __init__(self, pickle_root = None, http_push_url = None, http_push_key = None):
        threading.Thread.__init__(self)

        if http_push_url[-1] == '/':
            http_push_url = http_push_url[:-1]

        if http_push_url is not None:
            self.http_push_details = {
                'url': http_push_url,
                'key': http_push_key
            }
        else:
            self.http_push_details = None

        self.pickle_root = pickle_root
        self.isrunning = True
        self.stop = False

    def push_http_results(self):

        tmpfilespath = os.path.join(self.pickle_root, 'to_be_sent')
        picklefiles = [f for f in os.listdir(tmpfilespath) if os.path.isfile(os.path.join(tmpfilespath, f))]

        for file in picklefiles:
            print('Open: {}'.format(file))
            results = pickle.load(open(os.path.join(tmpfilespath, file), "rb"))
            if results is None:
                print('Error : pickle file is empty - {}'.format(os.path.join(tmpfilespath, file)))
                os.rename(os.path.join(tmpfilespath, file), os.path.join(os.path.join(self.pickle_root, 'data_error'), file))
                return

            if self.http_push_details is not None:
                if 'key' not in self.http_push_details or not self.http_push_details['key']:
                    print('Error: Authentication key is not set (Database issue)')
                    return

                if 'url' not in self.http_push_details or not self.http_push_details['url']:
                    print('Error: URL destination is not set (Database issue)')
                    return

                results['key'] = self.http_push_details['key']
                print('{}/restapi/set_test'.format(self.http_push_details['url']))
                r = requests.post('{}/restapi/set_test'.format(self.http_push_details['url']), json=results)
                if r.status_code != 200:
                    print('Error: cannot access the http route - code: {}'.format(r.status_code))
                    os.rename(os.path.join(tmpfilespath, file), os.path.join(os.path.join(self.pickle_root, 'http_error'), file))
                    return

                data = r.json()
                if data['status'] != 0:
                    print('Error: problem while pushing the results - code: {}'.format(data['status']))

                    log = os.path.join(os.path.join(self.pickle_root, 'data_error'), '{}_log.txt'.format(file[:-3]))
                    f = open(log, 'w')
                    f.write('Data errors (status {}): \n'.format(data['status']))
                    for err in data['errors']:
                        f.write('\t{}'.format(err))
                    f.close()

                    os.rename(os.path.join(tmpfilespath, file), os.path.join(os.path.join(self.pickle_root, 'data_error'), file))
                    return

                print('Pushed: {} file'.format(file))
                os.rename(os.path.join(tmpfilespath, file), os.path.join(os.path.join(self.pickle_root, 'success'), file))

            else:
                print('[Dev.] Push result function based on direct DB access is being developped')

    def is_running(self):
        return self.isrunning

    def stop(self):
        self.stop = True

    def run(self):
        if self.pickle_root is not None:
            while self.stop == False:
                self.push_http_results()
                time.sleep(1)

        self.isrunning = False
