import sys
import io
import time
import datetime
import gc
import psutil
import os
import importlib
import string
import random
import pickle
import traceback
import requests

import PSMSTestFramework.sysapi as sysapi

class StdoutHandler(io.StringIO):
    def __init__(self, queue, current_output):
        io.StringIO.__init__(self)
        self.terminal = current_output
        self.queue = queue

    def write(self, record):
        self.terminal.write(record)
        self.queue.put({'msg': 'stdout', 'str': record})

class StderrHandler(io.StringIO):
    def __init__(self, queue, current_output):
        io.StringIO.__init__(self)
        self.terminal = current_output
        self.queue = queue

    def write(self, record):
        #self.terminal.write(record)
        self.queue.put({'msg': 'stdout', 'str': record})

def push_results(results, pickle_root, http_push_details):
    if pickle_root is not None:
        today = datetime.datetime.now()
        while True:
            pickle_filename = '{}_{}'.format(today.strftime("%Y%m%d_%H%M%S"), ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(10)))
            pickle_file = os.path.join(pickle_root, pickle_filename)

            if not os.path.exists(pickle_file):
                break

        try:
            pickle.dump(results, open(pickle_file, "wb"))
            print('[Info] Pickle file stored: {}'.format(pickle_file))

        except Exception as e:
            dbStatus = -1
            dbErrTback = traceback.format_exc()
            dbErr = str(e)

            print(dbErrTback)

    elif http_push_details is not None:
        results['key'] = http_push_details['key']
        r = requests.post('{}/restapi/set_test'.format(http_push_details['url']), json=results)
        if r.status_code != 200:
            raise Exception('Error: cannot access the http route - code: {}'.format(r.status_code))

        data = r.json()
        if data['status'] != 0:
            raise Exception('Error: problem while pushing the results - code: {}'.format(data['status']))

    else:
        print('[Dev.] Push result function based on direct DB access is being developped')


def runner_report(stats, start, operator, tester_name, tester_version, test_name, log_file):
    today = datetime.datetime.now()

    if stats['run_cnt'] == 0:
        stats['run_cnt'] = 1

    status = ''

    status += '#----------------------------------------------------------------------------------------------------------\n'
    status += '#--- RUN LOG\n'
    status += '#----------------------------------------------------------------------------------------------------------\n'
    status += 'Report generated on    {}\n'.format(today.strftime("%Y.%m.%d %H:%M:%S"))
    status += '#----------------------------------------------------------------------------------------------------------\n'
    status += 'SOFTWARE:    {} - {}\n'.format(tester_name, tester_version)
    status += 'START TIME:  {}\n'.format(start.strftime("%Y.%m.%d %H:%M:%S"))
    status += 'END TIME:    {}\n'.format(today.strftime("%Y.%m.%d %H:%M:%S"))
    status += 'OPERATOR:    {}\n'.format(operator)
    status += 'RUN TYPE:    {}\n'.format(test_name)
    status += '#----------------------------------------------------------------------------------------------------------\n'
    status += '#----------------------------------------------------------------------------------------------------------\n'
    status += 'PASSED    {0: <10}    {1}%\n'.format(stats['passed_cnt'], round((100*float(stats['passed_cnt'])/float(stats['run_cnt'])),2))
    status += 'FAILED    {0: <10}    {1}%\n'.format(stats['failed_cnt'], round((100*float(stats['failed_cnt'])/float(stats['run_cnt'])),2))
    status += 'WARNING   {0: <10}    {1}%\n'.format(stats['warning_cnt'], round((100*float(stats['warning_cnt'])/float(stats['run_cnt'])),2))
    status += 'TOTAL     {0: <10}\n'.format(stats['run_cnt'])
    status += '#----------------------------------------------------------------------------------------------------------\n'
    status += '#----------------------------------------------------------------------------------------------------------\n'

    if len(stats['bins']) > 0:
        maxlen = 0
        for k in stats['bins'].keys():
            if len(k) > maxlen:
                maxlen = len(k)

        maxlen += 4

        for k in stats['bins'].keys():
            status += 'TOTAL {0: <{1}}{2}\n'.format(k,maxlen, stats['bins'][k])

        status += '#----------------------------------------------------------------------------------------------------------\n'
        status += '#----------------------------------------------------------------------------------------------------------\n'

    status += '#-- FAIL CAUSE ANALYSIS\n'
    maxlen = 0
    for k in stats['yield']:
        if len(k) > maxlen:
            maxlen = len(k)

    maxlen += 4

    status += '{0: <{1}}{2: <{3}}{4: <{5}}\n'.format('STATUS',maxlen,'TOTAL', 10, 'YIELD', 10)

    for k in stats['yield']:
        n = stats['yield'][k]
        name = '{0: <{1}}'.format(k, maxlen)
        cnt = '{0: <{1}}'.format(n, 10)
        yieldcalc = '{0: <{1}}'.format(round((100*float(n)/float(stats['run_cnt'])),2), 10)
        status += '{}{}{}%\n'.format(name, cnt, yieldcalc)

    for l in status.split('\n'):
        print('[Log]  {}'.format(l))

    if log_file is not None:
        try:
            file = open(log_file, 'w')
            file.write(status)
            file.close()
            print('[Log] Log saved: {}'.format(log_file))
        except Exception as e:
            print('[Error] Saving log failed: {}'.format(e))

    else:
        print(status)

def compute_statistics(stats, results):
    #Count runs
    stats['run_cnt'] += 1

    #Analyze status
    if results['pass'] == 0:
        stats['passed_cnt'] += 1

    elif results['pass'] > 0:
        stats['warning_cnt'] += 1

    else:
        stats['failed_cnt'] += 1

    #Analyze yield
    if results['pass'] == 0:
        stats['yield']['Success'] += 1

    else:
        for k in results['verboseDetails']:
            if k in stats['yield']:
                stats['yield'][k] += 1
            else:
                stats['yield'][k] = 1

        '''
        v = abs(results['pass'])
        for x in results['passDetails']:
            if (x['type'] == 'mask' and (v & x['value'])) or (x['type'] == 'value' and v == x['value']):
                if x['verbose'] not in stats['yield']:
                    stats['yield'][x['verbose']] = 1
                else:
                    stats['yield'][x['verbose']] += 1
        '''

    #Analyze bin
    if 'bin' in results:
        if results['bin'] in stats['bins']:
            stats['bins'][results['bin']] += 1
        else:
            stats['bins'][results['bin']] = 1

    #Return new stats
    return stats

def run(queue_out, queue_in, pause_event, stop_event, operator_id, function_type, bins, function, args, tester_name, tester_version, testinst_name, runner_log, pickle_root, http_push_details):

    #Import the package where the function is declared
    mod = importlib.import_module(function.__module__)
    importlib.reload(mod)

    #Redirect stderr and stdout
    tmp_stdout = sys.stdout
    tmp_stderr = sys.stderr
    sys.stdout = StdoutHandler(queue_out, tmp_stdout)
    sys.stderr = StderrHandler(queue_out, tmp_stderr)

    #Configure run
    sysapi.init_run(queue_out, queue_in, operator_id, pause_event, stop_event)

    #Statistics
    statistics = {
        'start_time': time.time(),
        'run_cnt': 0,
        'passed_cnt': 0,
        'warning_cnt': 0,
        'failed_cnt': 0,
        'yield': {'Success': 0},
        'bins': {}
    }

    #initialize bins
    for bin in bins:
        statistics['bins'][bin] = 0

    #First execution
    #file_object = open('time_report_gui.csv', 'w')
    #file_object.write('Run ID, Run duration (s), GC threshold0, GC thershold1, GC threshold2, Process memory\n')
    #file_object.close()

    runid = 0

    #Generate runner log filename
    today = datetime.datetime.now()

    if runner_log is not None:
        while True:
            log_filename = '{}_{}.txt'.format(today.strftime("%Y%m%d_%H%M%S"), ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(10)))
            log_file = os.path.join(runner_log, log_filename)

            if not os.path.exists(log_file):
                break

    while True:
        #Execute function
        start = datetime.datetime.today()
        try:
            ret = function(**args)
        except Exception as e:
            run_exception = traceback.format_exc()
            queue_out.put({'msg': 'exception', 'value': run_exception, 'title': str(e)})
            break

        end = datetime.datetime.today()
        print('[End.] Executed in {} seconds'.format((end-start).total_seconds()))
        runid += 1

        #Clear first run
        sysapi.clear_first_run()

        #Set 100%
        queue_out.put({'msg': 'progress', 'value': 100})

        #If function is different from debug, store results
        if function_type != sysapi.DEBUG_FUNCTION:
            try:
                push_results(ret, pickle_root, http_push_details)
            except Exception as e:
                queue_out.put({'msg': 'exception', 'value': '', 'title': str(e)})

        #If type is not runner, send result and exit
        if function_type != sysapi.RUNNER_FUNCTION:
            queue_out.put({'msg': 'result', 'value': ret})
            break

        #Compute stats
        statistics = compute_statistics(statistics, ret)
        queue_out.put({'msg': 'statistics', 'value': statistics})

        #Store log
        if function_type == sysapi.RUNNER_FUNCTION:
            runner_report(statistics, start, operator_id, tester_name, tester_version, testinst_name, log_file)

        #Check pause
        while pause_event.is_set() and not stop_event.is_set():
            print('[Pause] Wait for resume')
            time.sleep(1)

        #Check stop
        if stop_event.is_set():
            print('[Stop] Exit runner loop')
            break


