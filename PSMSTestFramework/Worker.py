import time
import graphviz
import base64

from multiprocessing import Process, Queue, Event
from sphinxcontrib import napoleon
from docutils.core import publish_string
from docutils.writers.html4css1 import Writer, HTMLTranslator
from docutils.parsers.rst import Directive, directives
from docutils import nodes

import PSMSTestFramework.sysapi as sysapi
import PSMSTestFramework.Runner as Runner

class FlowChartDirective(Directive):
    '''This `Directive` class tells the ReST parser what to do with the text it
    encounters -- parse the input, perhaps, and return a list of node objects.
    Here, usage of a single required argument is shown.
    See examples in docutils.parsers.rst.directives.*
    '''
    required_arguments = 0
    optional_arguments = 0
    has_content = True
    def run(self):
        #self.content
        try:
            content = '\n'.join(self.content)
            src = graphviz.Source(content)
            output = src.pipe(format='png')

            data_url_filetype = 'png'
            alt = 'flowchart'
            output = base64.b64encode(output).decode()
            data_path = "data:image/%s;base64,%s" % (data_url_filetype, output)
            attrs = []
            attrs.append('src="%s"' % data_path)
            attrs.append('alt="%s"' % alt)
            img = '<p style="text-align: center"><img %s /></p>' % ' '.join(attrs)
        except Exception as e:
            img = '<p style="text-align: center; color: orangee">Graphviz is not supported: {}</p>'.format(str(e))

        return [nodes.raw('', img, format='html')]

class Worker():
    def __init__(   self,
            db_host=None,
            db_username=None,
            db_password=None,
            db_port=None,
            db_dbname=None,

            pickle_tmp=None,

            http_push_url=None,
            http_push_key=None,

            tester_name='',
            tester_version='',

            runner_log=None
        ):

        self.function = None
        self.process = None
        self.progress = 0
        self.run_statistics = {}
        self.run_wid = 0
        self.tester_name = tester_name
        self.tester_version = tester_version
        self.runner_log = runner_log
        self.pickle_tmp = pickle_tmp

        self.statusQueue = Queue()
        self.setQueue = Queue()

        self.stdout_arr = []
        self.stdin_arr = []

        self.pause_event = Event()
        self.stop_event = Event()

        if http_push_url[-1] == '/':
            http_push_url = http_push_url[:-1]

        if http_push_url is not None:
            self.http_push_details = {
                'url': http_push_url,
                'key': http_push_key
            }
        else:
            self.http_push_details = None

        self.results = {
            'run_wid': -1,
            'type': -1,
            'data': {}
        }

    def execute(self, function, operator_id):
        if self.process is not None and self.process.is_alive():
            raise Exception('Runtime error: a function is already running');

        #Initialized
        self.progress = 0
        self.pause_event.clear()
        self.stop_event.clear()
        self.stdout_arr.clear()
        self.stdin_arr.clear()
        self.run_statistics = {
            'start_time': time.time(),
            'run_cnt': 0,
            'passed_cnt': 0,
            'warning_cnt': 0,
            'failed_cnt': 0,
            'yield': {'Success': 0},
            'bins': {}
        }

        self.run_wid = time.time()
        self.function = function

        #Clear queue: read all data from
        while not self.statusQueue.empty():
            r = self.statusQueue.get_nowait()

        #Prepare process
        workerProcess = Process(target=Runner.run, args=(
            self.statusQueue,
            self.setQueue,
            self.pause_event,
            self.stop_event,
            operator_id,
            function['type'],
            function['init_bins'],
            function['function'],
            function['args_value'],
            self.tester_name,
            self.tester_version,
            function['name'],
            self.runner_log,
            self.pickle_tmp,
            self.http_push_details
        ))

        self.process = workerProcess

        #Start process
        workerProcess.start()

    def toggle_pause(self):
        if self.pause_event.is_set():
            self.pause_event.clear()
            return False

        self.pause_event.set()
        return True

    def set_stop(self):
        self.stop_event.set()
        return True

    def read_queue(self):
        while not self.statusQueue.empty():
            r = self.statusQueue.get_nowait()

            if r['msg'] == 'progress':
                if 'fn' in r:
                    self.function['exec_fn'] = r['fn']
                else:
                    self.function['exec_fn'] = None
                self.progress = r['value']

            if r['msg'] == 'stdout':
                self.stdout_arr.append({'str': r['str'], 'timestamp': time.time()})
                if len(self.stdout_arr) > 500:
                    self.stdout_arr.pop(0)

            if r['msg'] == 'stdin':
                self.stdin_arr.append({'name': r['name'], 'desc': r['desc'], 'type': r['type'], 'timestamp': time.time()})

            if r['msg'] == 'stdin_multi':
                self.stdin_arr.append({'name': r['name'], 'params': r['params'], 'type': r['type'], 'timestamp': time.time()})

            if r['msg'] == 'statistics':
                self.run_statistics = r['value']

            if r['msg'] == 'result':

                #Get function documentation
                directives.register_directive('graphviz', FlowChartDirective)

                try:
                    function_doc = publish_string(self.function['function'].__doc__, writer_name='html').decode('utf-8')
                except Exception as e:
                    function_doc = str(e)

                self.results = {
                    'status': 0,
                    'run_wid': self.run_wid,
                    'type': self.function['type'],
                    'data': r['value'],
                    'desc': function_doc,
                    'name': self.function['name']
                }

            if r['msg'] == 'exception':
                self.results = {
                    'status': -1,
                    'run_wid': self.run_wid,
                    'title': r['title'],
                    'exception': r['value']
                }

    def get_result_wid(self):
        return self.results['run_wid']

    def get_result(self):
        return self.results

    def stdout(self):
        #Get queue
        self.read_queue()
        return self.stdout_arr

    def stdin(self):
        #Get queue
        self.read_queue()
        return self.stdin_arr

    def get_statistics(self):
        return self.run_statistics

    def push_stdin(self, param_name, value):
        for i in range(len(self.stdin_arr)):
            if self.stdin_arr[i]['name'] == param_name:
                self.stdin_arr.pop(i)
                self.setQueue.put({'name': param_name, 'value': value})
                return 0

        return -1

    def status(self):
        #Get queue
        self.read_queue()

        #Format name
        if self.function is None:
            name = ''
        elif 'exec_fn' in self.function and self.function['exec_fn']:
            name = '{} > {}'.format(self.function['name'], self.function['exec_fn'])
        else:
            name = self.function['name']

        return {
            'is_running': False if self.process is None else self.process.is_alive(),
            'pause_status': self.pause_event.is_set(),
            'stop_status': self.stop_event.is_set(),
            'name': name,
            'type': '' if self.function is None else self.function['type'],
            'progress': self.progress,
            'statistics': self.run_statistics
        }