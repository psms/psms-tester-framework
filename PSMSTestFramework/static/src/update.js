var update = function(button_id, update_callback){

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    var root = this;
    var btn_id = button_id;
    var updt_cback = update_callback;

    /*
     * Constructor
     */
    this.construct = function(){
        window.setInterval(function(){
            check_for_update();
        }, 600000);
        check_for_update();

        $('body').append(''+
            '<div id="update-done-modal" class="w3-modal">'+
            '  <div class="w3-modal-content">'+
            '    <div class="w3-container w3-padding">'+
            '        <span onclick="document.getElementById(\'update-done-modal\').style.display=\'none\'"  class="w3-button w3-display-topright">&times;</span>'+
            '        <h4>Update</h4>'+
            '        <div class="w3-row">'+
            '            <div id="update-msg" style="text-align: justify">'+
            '            </div>'+
            '            <div id="update-btn-show" style="text-align: center"><button id="'+btn_id+'_update" class="w3-button w3-border"><b>Upgrade</b></button></div>'+
            '        </div>'+
            '    </div>'+
            '  </div>'+
            '</div>');


        $('#'+btn_id).click(function(){
            $('#update-done-modal').fadeIn(200);
        });

        $('#'+btn_id+'_update').click(function(){
            $('#update-btn-show').hide();
            $('#update-done-modal').fadeOut(200);

            $('#'+btn_id).attr("disabled", true);
            $('#'+btn_id).html('In progress...');

            fetch('/update', {
                method: 'GET',
                credentials: 'include'
            })

            .then( (response) => {

                $('#'+btn_id).removeAttr("disabled");
                $('#'+btn_id).html('<b>Update</b>');

                if(!response.ok){
                    printError('<b>Server error:</b> HTTP '+response.status);
                    return null;
                }
                return response.json()
            })

            .then( (json) => {
                if(json == null) return;

                if(json['status'] == -1){
                    printError('<b>Package installation error:</b> <pre style="margin-top:16px"><code>'+json['msg']+'</code></pre><p><b>Warning:</b> Automatically reverted to the previous commit</p>');
                    return null;
                }
                else if(json['status'] < 0){
                    printError('<b>Server error:</b> '+json['error']+'<pre style="margin-top:16px"><code>'+json['traceback']+'</code></pre>')
                    return null;
                }else{
                    $('#'+btn_id).hide();
                    printSuccess('Repository has been succesfully updated');
                    updt_cback();
                }
            })
        });

    };

    var check_for_update = function(){
        fetch('/version', {
            method: 'GET',
            credentials: 'include'
        })

        .then( (response) => {
            if(!response.ok){
                $('#'+btn_id).hide();
                return null;
            }
            return response.json()
        })

        .then( (json) => {
            if(json == null){
                $('#'+btn_id).fadeOut(200);
            }
            else if(json != null && json['status'] != 0){
                $('#'+btn_id).fadeOut(200);
                //printError(json['msg']);
            }else if(json != null && json['available'] == true){
                $('#'+btn_id).fadeIn(200);

                var modalHTML = '';

                $('#update-btn-show').hide();

                if(json['modification_status'] == 1){
                    modalHTML += '<div class="w3-orange w3-container w3-card" style="padding-top: 16px; padding-bottom: 16px"><b>Warning:</b> The local commit is newer than the server one.</div>';
                }
                else if(json['modification_status'] == 0){
                    if(json['merge_conflicts'].length == 0){
                        modalHTML += '<div class="w3-green w3-container w3-card" style="padding-top: 16px; padding-bottom: 16px"><b>Ready:</b> Server contains a more recent version and can be updated.</div>';

                        $('#update-btn-show').show();
                    }else{
                        modalHTML += '<div class="w3-red w3-container w3-card" style="padding-top: 16px; padding-bottom: 16px"><b>Error:</b> Server modification detected but there is, at least, one merge conflict. Automatic upgrade cannot be done.</div>';
                    }
                }

                if(json['merge_conflicts'].length > 0){
                    modalHTML += '<div class="w3-container w3-margin-top">'+
                        '<b class="w3-text-red">Merge conflict(s)</b>:<ul>'+
                        json['merge_conflicts'].join('')+
                        '</ul></div>';
                }

                modalHTML += '<div class="w3-container w3-margin-top">'+
                    '<b>Update report</b>:<ul>'+
                    '<li>Server:<ul><li>SHA: '+json['server']['sha']+'</li><li>Date: '+json['server']['date']+'</li></ul></li>'+
                    '<li>Local:<ul><li>SHA: '+json['local']['sha']+'</li><li>Date: '+json['local']['date']+'</li></ul></li>'+
                    '</ul></div>';

                if(json['file_changed'].length > 0){
                    modalHTML += '<div class="w3-container w3-margin-top">'+
                        '<b>Server difference</b>:<ul>'+
                        json['file_changed'].join('')+
                        '</ul></div>';
                }

                if(json['not_commited'].length > 0){
                    modalHTML += '<div class="w3-container w3-margin-top">'+
                        '<b>Local modification(s) not commited</b>:<ul>'+
                        json['not_commited'].join('')+
                        '</ul></div>';
                }

                $('#update-msg').html(modalHTML);

            }else{
                $('#'+btn_id).fadeOut(200);
            }
        })
    };

    var printError = function(msg){
        $('#update-msg').html('<div class="w3-red w3-container w3-card w3-margin-bottom" style="padding-top: 16px; padding-bottom: 16px"><b>Error:</b> '+msg+'</div>');
        $('#update-done-modal').fadeIn(200);
    }

    var printSuccess = function(msg){
        $('#update-msg').html('<div class="w3-green w3-container w3-card w3-margin-bottom" style="padding-top: 16px; padding-bottom: 16px"><b>Success:</b> '+msg+'</div>');
        $('#update-done-modal').fadeIn(200);
    }

    var printWarning = function(msg){
        $('#update-msg').html('<div class="w3-green w3-container w3-card w3-margin-bottom" style="padding-top: 16px; padding-bottom: 16px"><b>Success:</b> '+msg+'</div>');
        $('#update-done-modal').fadeIn(200);
    }

    this.construct();

};
