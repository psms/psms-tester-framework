function format(id, type, disp, data){
    if(type == 'table')       return format_table(id, disp, data);
    if(type == 'scatter')     return format_scatter(id, disp, data);

    return {
        'html': '',
        'id': '',
        'js': function(){return;}
    };
}