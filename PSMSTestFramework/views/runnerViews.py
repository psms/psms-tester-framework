import json

from flask import redirect
from flask import request
from flask import session

import PSMSTestFramework.sysapi as sysapi

class RunnerViews():
    '''
    test Views.
    '''

    def __init__(
            self,
            application,
            functionListGetter,
            worker
        ):

        self.application = application
        self.functionListGetter = functionListGetter
        self.worker = worker

        self.application.add_url_rule('/run', 'run', self.runfn, methods=['POST'])
        self.application.add_url_rule('/status', 'status', self.status, methods=['GET'])
        self.application.add_url_rule('/stdout', 'stdout', self.stdout, methods=['POST'])
        self.application.add_url_rule('/stdin', 'get_stdin_req', self.get_stdin_req, methods=['GET'])
        self.application.add_url_rule('/stdin', 'set_stdin_req', self.set_stdin_req, methods=['POST'])
        self.application.add_url_rule('/pause', 'pause', self.pause, methods=['GET'])
        self.application.add_url_rule('/stop', 'stop', self.stop, methods=['GET'])
        self.application.add_url_rule('/resultwid', 'resultwid', self.resultwid, methods=['GET'])
        self.application.add_url_rule('/result', 'result', self.result, methods=['GET'])

    def result(self):
        return json.dumps({'status': 0, 'result': self.worker.get_result()})

    def resultwid(self):
        return json.dumps({'status': 0, 'run_wid': self.worker.get_result_wid()})

    def pause(self):
        return json.dumps({'status': 0, 'state': self.worker.toggle_pause()})

    def stop(self):
        return json.dumps({'status': 0, 'state': self.worker.set_stop()})

    def status(self):
        return json.dumps(self.worker.status())

    def get_stdin_req(self):
        stdin = self.worker.stdin()
        return json.dumps({'status': len(stdin), 'inputs': stdin})

    def set_stdin_req(self):
        data = request.json
        return json.dumps({'status': self.worker.push_stdin(data['name'], data['value'])})

    def stdout(self):
        data = request.json

        stdout_arr = self.worker.stdout()
        stdout_str = ''
        stdout_lasttime = 0

        for s in stdout_arr:
            if s['timestamp'] > data['timestamp']:
                stdout_str += s['str']
                stdout_lasttime = s['timestamp']

        return json.dumps({'status': 0 if stdout_lasttime > 0 else -1, 'lasttime': stdout_lasttime, 'str': stdout_str})

    def runfn(self):
        '''
        The runfn function receives a JSON body from the HTTP request describing the debug/test function
        to be ran.

        Args:
            JSON:

                .. code-block:: javascript

                    {
                        'pkg': function_package,
                        'fn': function_name,
                        'params': param_dict
                    }

        Returns:
            .. code-block:: javascript

                /** When function is added to queue */
                {
                    'status': 0
                }

                /** When function a function is already running */
                {
                    'status': -1,
                    'srt': 'Runtime error: a function is already running',
                    'traceback': traceback
                }

                /** In case of wrong parameter */
                {
                    'status': -1,
                    'srt': 'Param ... is not set but required',
                    'traceback': traceback
                }
        '''
        data = request.json

        #Get details
        try:
            d_id = int(data['id'])
            d_params = data['params']
        except Exception as e:
            return json.dumps({'status': -1, 'error': str(e), 'traceback':''})

        #Search for function
        function_list = self.functionListGetter()
        function_sel = None

        for f in function_list['functions']:
            if f['id'] == d_id:
                function_sel = f

        if function_sel is None:
            return json.dumps({'status': -2, 'error': 'function not found', 'traceback':''})

        #Check params
        f_params = function_sel['params']

        args_value = {}

        for p in f_params:
            if p not in d_params or not d_params[p]:
                return json.dumps({'status': -3, 'error': 'Param {} is not set but required'.format(p), 'traceback':''})

            args_value[p] = d_params[p]

        if len(f_params) != len(args_value):
            return json.dumps({'status': -3, 'error': 'Params are not set but required {}'.format(f_params), 'traceback':''})

        function_sel['args_value'] = args_value

        #Execute worker
        try:
            self.worker.execute(function_sel, session['username'])
        except Exception as e:
            return json.dumps({'status': -1, 'msg': str(e)})

        return json.dumps({'status': 0})