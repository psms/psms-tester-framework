import random
import time
import numpy as np

import PSMSTestFramework

MIN_HIGH_DET_FAILURE = 0x01
MAX_ZERO_DET_FAILURE = 0x02
MIN_HIGH_SET_FAILURE = 0x04
MAX_ZERO_SET_FAILURE = 0x08

def get_gpio_state(input_v):
    """Emulate a GPIO read action

    This function evaluates the GPIO value depending on the input voltage of
    the pin. To emulate errors, the pin value is randomized with probability
    factors depending on the input voltage.

    Args:
        input_v: input voltage.

    Returns:
        Returns the pin state (0: low / 1: high)
    """
    if input_v < 0.5:
        return int(np.random.choice(np.array([0, 1]), p=[0.95, 0.05]))
    elif input_v < 0.8:
        return int(np.random.choice(np.array([0, 1]), p=[0.5, 0.5]))
    else:
        return int(np.random.choice(np.array([0, 1]), p=[0.05, 0.95]))

def get_gpio_output_voltage(output_l):
    """Emulate a GPIO set action

    This functions evaluates a measured voltage on the output of the pin. In
    order to accurate the emulation, the voltage output depends on the load.
    In addition, to simulate wrong behaviours, the output voltage is randomized
    using probability factors debending on the output load.

    Args:
        output_l: output load (in ohms).

    Returns:
        Returns the output voltage that could be measured at the output of the pin.
    """
    if output_l < 3:
        return float(
            np.random.choice(
                np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]),
                p=[0.50, 0.20, 0.15, 0.06, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01]
            )
        )
    elif output_l < 3.5:
        return float(
            np.random.choice(
                np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]),
                p=[0.01, 0.01, 0.01, 0.01, 0.01, 0.07, 0.10, 0.50, 0.10, 0.10, 0.06, 0.01, 0.01]
            )
        )
    else:
        return float(
            np.random.choice(
                np.array([0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2]),
                p=[0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.06, 0.15, 0.20, 0.50]
            )
        )

@PSMSTestFramework.sysapi.register_debug('Example [GPIO Subtest]')
def gpio_subtest(io_id, input_min_voltage, input_max_voltage, input_steps, output_min_load, output_max_load, output_steps, high_voltage_threshold):
    """Test a specific I/O pin

    This function test a specific GPIO depending on different parameters.
    The flowchart below describe the testing procedure:

    .. graphviz::

        digraph G {
            node [fontname = "Handlee"];
            edge [fontname = "Handlee"];

            start [
                label = "Start";
                shape = oval;
            ];

            init_inputv [
                label = "inputV = input_min_voltage";
                shape = rect;
            ];

            inputv_inrange [
                label = "inputV";
                shape = diamond;
            ];

            get_gpio_state [
                label = "Set input voltage and get GPIO state";
                shape = rect;
            ];

            is_iohigh [
                label = "I/O = 1";
                shape = diamond;
            ];

            is_minhigh_det [
                label = "min_high_det not set";
                shape = diamond;
            ];

            set_minhigh_det [
                label = "min_high_detect = inputV";
                shape = rect;
            ];

            set_maxzer_det [
                label = "max_zero_detect = inputV";
                shape = rect;
            ];

            inc_inputv [
                label = "inputV += input_steps";
                shape = rect;
            ];

            init_outputl [
                label = "outputL = output_min_load";
                shape = rect;
            ];

            outputl_inrange [
                label = "outputL";
                shape = diamond;
            ];

            get_gpio_outvoltage [
                label = "Set load and get GPIO output voltage";
                shape = rect;
            ];

            is_ouputv [
                label = "output_v?";
                shape = diamond;
            ];

            set_minhigh_set [
                label = "min_high_set = output_v";
                shape = rect;
            ];

            is_minhight_set [
                label = "min_high_set not set";
                shape = diamond;
            ];

            set_maxzer_set [
                label = "max_zero_set = output_v";
                shape = rect;
            ];

            inc_outputl [
                label = "outputL += output_steps";
                shape = rect;
            ];

            end [
                label = "End";
                shape = oval;
            ];

            start -> init_inputv
            init_inputv -> inputv_inrange
            inputv_inrange:w -> get_gpio_state:n [label = "<= input_max_voltage"];
            get_gpio_state:s -> is_iohigh:n
            is_iohigh:e -> set_maxzer_det:w [label = "No"];
            is_iohigh:s -> is_minhigh_det:n [label = "Yes"];
            is_minhigh_det:s -> set_minhigh_det:n [label = "Yes"]
            is_minhigh_det:e -> inc_inputv:e [label = "No"]
            set_maxzer_det:s -> inc_inputv:e
            set_minhigh_det:s -> inc_inputv
            inc_inputv:s -> init_outputl:w
            inputv_inrange:e -> init_outputl:n [label = "else"];
            init_outputl -> outputl_inrange
            outputl_inrange:w -> get_gpio_outvoltage:n [label = "<= output_max_load"];
            get_gpio_outvoltage:s -> is_ouputv:n
            is_ouputv:e -> set_maxzer_set:w [label = "< high_voltage_threshold"];
            is_ouputv:s -> is_minhight_set:n [label = ">= high_voltage_threshold"];
            is_minhight_set:s -> set_minhigh_set:n [label = "Yes"]
            is_minhight_set:e -> inc_outputl:e [label = "No"]
            set_maxzer_set:s -> inc_outputl:e
            set_minhigh_set:s -> inc_outputl
            outputl_inrange:e -> end:n [label = "else"];
            inc_outputl -> end

            {
                rank=same;
                is_iohigh; set_maxzer_det;
            }
            {
                rank=same;
                is_ouputv; set_maxzer_set;
            }
        }


    Args:
        io_id: specify the pin id to be tested
        input_min_voltage: minimum input voltage to be set during the input mode testing (in volt)
        input_max_voltage: maximum input voltage to be set during the input mode testing (in volt)
        input_steps: step size for the input mode testing (in volt)
        output_min_load: minimum output load to be set during the output mode testing (in ohm)
        output_max_load: maximum output load to be set during the output mode testing (in ohm)
        output_steps: step size for the output mode testing (in ohm)
        high_voltage_threshold: threshold voltage to consider the output to be high

    Returns:
        The gpio_subtest function returns all of the test details formatted like described below

        .. code-block:: javascript

            {
                'test_type_name': 'GPIO_EMUL_v0',
                'name': 'gpio_{}'.format(io_id),

                'details': {
                    'io_id': io_id,
                    'input_min_voltage': input_min_voltage,
                    'input_max_voltage': input_max_voltage,
                    'input_steps': input_steps,
                    'output_min_load': output_min_load,
                    'output_max_load': output_max_load,
                    'output_steps' : output_steps,
                    'high_voltage_threshold': high_voltage_threshold
                },

                'summary': {
                    'min_high_detect': MIN_HIGH_DETECT,
                    'max_zero_detect': MAX_ZERO_DETECT,
                    'min_high_set': MIN_HIGH_SET,
                    'max_zero_set': MAX_ZERO_SET,
                },

                'measurements': MEASUREMENTS,
                'pass': pass_v
            }
    """

    MEASUREMENTS = []
    MIN_HIGH_DETECT = -1
    MAX_ZERO_DETECT = -1
    MIN_HIGH_SET = -1
    MAX_ZERO_SET = -1

    #Check input:
    input_voltages = np.arange(float(input_min_voltage), float(input_max_voltage), float(input_steps)).tolist()

    for input_v in input_voltages:
        gpio_in = get_gpio_state(input_v)

        MEASUREMENTS.append({
            'name': 'input_voltage_detect',
            'data': {'v': input_v, 'r': gpio_in},
            'pass': -1
        })

        if gpio_in == 1 and MIN_HIGH_DETECT == -1:
            MIN_HIGH_DETECT = input_v

        if gpio_in == 0:
            MAX_ZERO_DETECT = input_v

    #Check output:
    output_loads = np.arange(float(output_min_load), float(output_max_load), float(output_steps)).tolist()

    for output_l in output_loads:
        output_v = get_gpio_output_voltage(output_l)

        MEASUREMENTS.append({
            'name': 'output_load_test',
            'data': {'l': output_l, 'v': output_v},
            'pass': -1
        })

        if output_v > float(high_voltage_threshold) and MIN_HIGH_SET == -1:
            MIN_HIGH_SET = output_v

        if output_v < float(high_voltage_threshold):
            MAX_ZERO_SET = output_v

    #Check pass/fail
    pass_v = 0x00

    if MIN_HIGH_DETECT < 0.5:
        pass_v |= MIN_HIGH_DET_FAILURE

    if MAX_ZERO_DETECT > 0.8:
        pass_v |= MAX_ZERO_DET_FAILURE

    if MIN_HIGH_SET < float(high_voltage_threshold):
        pass_v |= MIN_HIGH_SET_FAILURE

    if MAX_ZERO_SET >= float(high_voltage_threshold):
        pass_v |= MAX_ZERO_SET_FAILURE

    return {
            'test_type_name': 'GPIO_EMUL_v0',
            'name': 'gpio_{}'.format(io_id),

            'details': {
                'io_id': io_id,
                'input_min_voltage': float(input_min_voltage),
                'input_max_voltage': float(input_max_voltage),
                'input_steps': float(input_steps),
                'output_min_load': float(output_min_load),
                'output_max_load': float(output_max_load),
                'output_steps' : float(output_steps),
                'high_voltage_threshold': float(high_voltage_threshold)
            },

            'summary': {
                'min_high_detect': MIN_HIGH_DETECT,
                'max_zero_detect': MAX_ZERO_DETECT,
                'min_high_set': MIN_HIGH_SET,
                'max_zero_set': MAX_ZERO_SET,
            },

            'measurements': MEASUREMENTS,
            'pass': -pass_v
    }