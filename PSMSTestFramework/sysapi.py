from io import StringIO
import time
import traceback
import importlib
import inspect
import os
import sys
import uuid

FUNCTIONS = []

TEST_FUNCTION = 0
DEBUG_FUNCTION = 1
RUNNER_FUNCTION = 2

RAW_INPUT = 0
RAW_MULTI_INPUT = 2
YESNO_INPUT = 1

#Decorators
def register_test(name, init_bins=[]):
    def wrapper(f):
        FUNCTIONS.append({'type': TEST_FUNCTION, 'name': name, 'function': f, 'init_bins': init_bins})
        return f
    return wrapper

def register_debug(name,init_bins=[]):
    def wrapper(f):
        FUNCTIONS.append({'type': DEBUG_FUNCTION, 'name': name, 'function': f, 'init_bins': init_bins})
        return f
    return wrapper

def register_runner(name,init_bins=[]):
    def wrapper(f):
        FUNCTIONS.append({'type': RUNNER_FUNCTION, 'name': name, 'function': f, 'init_bins': init_bins})
        return f
    return wrapper

def get_registered_functions():
    global FUNCTIONS
    return FUNCTIONS

def clear_registered_functions():
    global FUNCTIONS
    FUNCTIONS.clear()

#Tool functions
def get_tester_functions(tests_root):

    functions = []
    errors = []
    id = 0

    for (dirpath, dirnames, filenames) in os.walk(tests_root):
        for f in filenames:
            if f[-3:] == '.py':
                package = f.replace('./','')[:-3]
                package = package.replace('.\\','')
                package = package.replace('/','.')
                package = package.replace('\\','.')

                try:
                    clear_registered_functions()
                    mod = importlib.import_module(package)

                    #importlib.reload(mod)

                    #List functions
                    testlist = get_registered_functions()

                    for t in testlist:
                        params_tmp = t['function'].__code__.co_varnames[0:(t['function'].__code__.co_argcount)]

                        params = []
                        for p in params_tmp:
                            if p != 'self':
                                params.append(p)

                        functions.append({
                            'id': id,
                            'type': t['type'],
                            'package': package,
                            'name': t['name'],
                            'function': t['function'],
                            'init_bins': t['init_bins'],
                            'params': params
                        })

                        id += 1

                except Exception as e:
                    errors.append({'package': package, 'error': str(e), 'traceback': traceback.format_exc()})

    return {'functions': functions, 'errors': errors}

#Data exchange functions
class RunConfiguration():

    def __init__(self, queue_out, queue_in, operator_id, paused_event, stopped_event):
        self.queue_out = queue_out
        self.queue_in = queue_in
        self.operator_id = operator_id
        self.first_run = True
        self.paused_event = paused_event
        self.stopped_event = stopped_event

        #Flush queue_in
        while not queue_in.empty():
            r = queue_in.get_nowait()

    def queue_put(self, msg):
        self.queue_out.put(msg)

    def queue_get(self):
        return self.queue_in.get()

    def is_stopped(self):
        return self.stopped_event.is_set()

    def is_paused(self):
        return self.paused_event.is_set()

    def is_initialized(self):
        return not self.first_run

    def get_operator(self):
        return self.operator_id

    def clear_first_run(self):
        self.first_run = False

runConfigInst = None

def init_run(queue_out, queue_in, operator_id, paused_event, stopped_event):
    global runConfigInst
    runConfigInst = RunConfiguration(queue_out, queue_in, operator_id, paused_event, stopped_event)

def queue_put(msg):
    global runConfigInst
    runConfigInst.queue_put(msg)

def queue_get():
    global runConfigInst
    return runConfigInst.queue_get()

def set_job_progress(p):
    queue_put({'msg': 'progress', 'fn': inspect.stack()[1][3], 'value': p})

def get_multi_data(params):
    id = uuid.uuid4().hex

    queue_put({'msg': 'stdin_multi', 'name': id, 'params': params, 'type': RAW_MULTI_INPUT})

    print('[Pause] Wait for data from web interface: {}'.format(id))
    while True:
        r = queue_get()
        if r['name'] == id:
            return r['value']

def get_data(param_name, param_desc=''):

    queue_put({'msg': 'stdin', 'name': param_name, 'desc': param_desc, 'type': RAW_INPUT})

    print('[Pause] Wait for data from web interface: {}'.format(param_name))
    while True:
        r = queue_get()
        if r['name'] == param_name:
            return r['value']

def get_yesno(param_name, param_desc=''):
    queue_put({'msg': 'stdin', 'name': param_name, 'desc': param_desc, 'type': YESNO_INPUT})
    print('[Pause] Wait for data from web interface: {}'.format(param_name))
    while True:
        r = queue_get()
        if r['name'] == param_name:
            return r['value']

def operator_id():
    global runConfigInst
    return runConfigInst.get_operator()

def is_initialized():
    global runConfigInst
    return runConfigInst.is_initialized()

def is_stopped():
    global runConfigInst
    return runConfigInst.is_stopped()

def clear_first_run():
    global runConfigInst
    return runConfigInst.clear_first_run()
