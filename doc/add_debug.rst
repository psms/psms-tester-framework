Debug functions
***************

PSMSFramework supports two kinds of functions that can be called via the GUI: test or debug.

The debug procedures are made to execute specific tests to validate a feature functionality of the device. It can be considered as a subtest of a test sequence.
Because the functions are called to manually check a part of the device, the results are not pushed to the database and don't need specific display information.
In this case, the Graphical User Interface (GUI) is only displaying the function documenation and its result.

Python script
=============

The example below shows how to implement a debug function in a file.

.. code-block:: python

    import PSMSFramework
    import random

    @PSMSFramework.sysapi.register_debug('Debug function Name')
    def function_dbg(intfId, minvalue, maxvalue):
        '''
        Debug function description:

        As it is the case on the top comment for the test, it is a good method
        to describe the debug function here. This description will be print with
        the debug function result. It also support the graphviz extension as
        mentioned before. In addition, it is recommended to describe the function
        parameters and the return value.

        For example, this function generate 100 random values, display them in
        the console and returns the subtest result.

        Args:
            :intfId: interface identifier
            :minvalue: minimum value for random integer
            :maxvalue: maximum value for random integer

        Returns:
            This function returns the parameters set in input as an example:

            .. code-block:: javascript

                {
                    'details': {
                        'intfId': intfId,
                        'minvalue': minvalue,
                        'maxvalue': maxvalue
                    },

                    'summary': {
                        'avg':(sum(randArr)/len(randArr)),
                        'max': max(randArr),
                        'min': min(randArr)
                    },

                    'measurements': meas,
                    'pass': 0
                }
        '''

        #Print a message in the console
        print('[info] Debug function called')

        #Measurement points variable
        meas = []
        randArr = []

        #Emulate a measurement
        for i in range(100):
            v = random.randint(int(minvalue),int(maxvalue))
            randArr.append(v)

            #Print the random value
            print('[meas] Random value ({}): {}'.format(i, v))

            #Store formatted measurement
            meas.append({
                    'name': 'random_val',
                    'data': {'v': v},
                    'pass': 0
                })

        #Returns the subtest result
        return {
                'details': {
                    'intfId': intfId,
                    'minvalue': minvalue,
                    'maxvalue': maxvalue
                },

                'summary': {
                    'avg': (sum(randArr)/len(randArr)),
                    'max': max(randArr),
                    'min': min(randArr)
                },

                'measurements': meas,
                'pass': 0
            }

The ``@PSMSFramework.sysapi.register_debug`` decorator registers the debug function in the system and make it available on the interface.
Its argument defines the function name that will be displayed in the selection list.

Repository architecture
=======================

Then, in the example, the tester repository looks like:

.. figure:: img/debug_tester_repo.png
   :align:   center

   Repository architecture

.. note::

    The main.py file contains the code introduced in the "How-to start" section and the debug_example.py the one above.

.. note::

    Files name are totally random in the example, they have no impact on the functionality and can be called differently if needed.

Tester execution
================

Finally, after starting the main.py script using the ``python3 main.py`` command, the tester can be accessed via the usual web browser:

.. thumbnail:: img/debug_browser_beforerun.png
   :align:   center

   GUI before running the debug function

After selecting the debug function in the 'What to do?' list, the interface automatically detects that two arguments have to be passed and asks the user for them.
After filling the form, the function can be ran, providing the following result:

.. thumbnail:: img/debug_browser_result.png
   :align:   center

   Debug function result

And the console section reports the print message has expected:

.. thumbnail:: img/debug_browser_console.png
   :align:   center

   Debug function console