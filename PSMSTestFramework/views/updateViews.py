import json
import git
import time

from flask import redirect
from flask import request
from flask import session

from authlib.integrations.flask_client import OAuth

import PSMSTestFramework.sysapi as sysapi

class UpdateViews():
    '''
    test Views.
    '''

    def __init__(
            self,
            application,
            git_root,
        ):

        self.application = application
        self.git_root = git_root

        self.application.add_url_rule('/version', 'get_version', self.get_version, methods=['GET'])
        self.application.add_url_rule('/update', 'update_version', self.update_version, methods=['GET'])

    def get_version(self):
        '''
        The get_version functions initialize a git repo variable from the git_root folder and get
        the local commit SHA. Then it fetches the repo to get the latest server information,
        get the head commit SHA and compare both to know whether the tester can be updated or
        not.

        Returns:
            .. code-block:: javascript

                /** When git repository exists */
                {
                    'status': 0,
                    'available': (sever_commit.hexsha != local.hexsha),
                    'server': sever_commit.hexsha,
                    'local': local.hexsha
                }


                /** When tester is not a git repository */
                {
                    'status': -1,
                    'repo': self.git_root,
                    'msg': str(e)
                }

        '''

        try:
            repo = git.Repo(self.git_root)
        except Exception as e:
            return json.dumps({'status': -1, 'repo': self.git_root, 'msg': str(e)})

        local = repo.commit()

        if repo.active_branch.name != 'master':
            return json.dumps({'status': -2, 'msg': 'Only the master branch supports automatic upgrade', 'branch': repo.active_branch.name})

        if len(repo.remotes.origin.fetch()):
            sever_commit = repo.remotes.origin.fetch()[0].commit
        else:
            return json.dumps({'status': -1, 'msg': 'Server connection failed', 'repo': self.git_root, 'local': local.hexsha})

        #Local modification
        if local.committed_date > sever_commit.committed_date:
            modification_status = 1

        #Remote modification
        elif local.committed_date == sever_commit.committed_date:
            modification_status = 2

        #Up to date
        else:
            modification_status = 0

        #List changed files between commit
        file_changed = []
        diff = local.diff(sever_commit)

        for x in diff:
            if x.a_blob and x.a_blob.path not in file_changed:
                file_changed.append(x.a_blob.path)

            if x.b_blob is not None and x.b_blob.path not in file_changed:
                file_changed.append(x.b_blob.path)

        not_commited = [ item.a_path for item in repo.index.diff(None) ]
        not_commited.extend(repo.untracked_files)

        merge_conflicts = []

        for x in file_changed:
            if x in not_commited:
                merge_conflicts.append(x)

        html_not_commited = []
        for x in not_commited:
            html_not_commited.append('<li>'+x.replace('/',' <b>&#62;</b> ')+'</li>')

        html_merge_conflicts = []
        for x in merge_conflicts:
            html_merge_conflicts.append('<li>'+x.replace('/',' <b>&#62;</b> ')+'</li>')

        html_file_changed = []
        for x in file_changed:
            html_file_changed.append('<li>'+x.replace('/',' <b>&#62;</b> ')+'</li>')

        return json.dumps({
                'status': 0,
                'available': (sever_commit.hexsha != local.hexsha),
                'modification_status': modification_status,
                'server': {
                    'sha': sever_commit.hexsha,
                    'date': time.strftime("%a, %d %b %Y %H:%M", time.gmtime(sever_commit.committed_date))
                },
                'local': {
                    'sha': local.hexsha,
                    'date': time.strftime("%a, %d %b %Y %H:%M", time.gmtime(local.committed_date))
                },

                'not_commited': html_not_commited,
                'file_changed': html_file_changed,
                'merge_conflicts': html_merge_conflicts
            })

    def update_version(self):
        '''
        The update function pull the latest version of the git repository, checks the requirements file and install dependancies if they changed.
        In case of failure during a package installation, it automatically rollback to the previous version.

        Returns:
            .. code-block:: javascript

                /** When succeed */
                {
                    'status': 0
                }

                /** When a dependency installation fails */
                {
                    'status': -1,
                    'msg': exc.output
                }

        '''
        repo = git.Repo(self.git_root)

        #Store current commit (used to check difference on requirements.txt file)
        prev_version = repo.commit()

        #Pull latest version
        o = repo.remotes.origin
        o.pull()

        #Get new commit
        curr_version = repo.commit()

        #List changed files
        req_file_changed = []
        diff = prev_version.diff(curr_version)

        for x in diff:
            if x.a_blob and x.a_blob.path not in req_file_changed:
                if 'requirements.txt' in x.a_blob.path:
                    req_file_changed.append(x.a_blob.path)

            if x.b_blob is not None and x.b_blob.path not in req_file_changed:
                if 'requirements.txt' in x.b_blob.path:
                    req_file_changed.append(x.b_blob.path)

        #Install packages if needed
        for f in req_file_changed:
            try:
                output = subprocess.check_output(
                    [sys.executable, "-m", "pip", "install", "-r", '{}/{}'.format(self.git_root, f).replace('//','/')],
                    stderr=subprocess.STDOUT, shell=True, timeout=3,
                    universal_newlines=True)
            except subprocess.CalledProcessError as exc:
                #In case of failure, go back to previous commit
                repo.git.reset('--hard',prev_version.hexsha)

                return json.dumps({'status':-1, 'msg': exc.output})

        return json.dumps({'status':0})