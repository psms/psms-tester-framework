Introduction
************

This section introduces the Production and Support Management System (PSMS) with a dedicated focus on the PSMSFramework package.

PSMS System
===========

The PSMS (Product and Support Management System) project aims to provide a set of tools to ease the production and support for custom product designed at CERN. It offers interfaces to administrate all of the tasks that follow the development stage. The Figure 1 below introduces all of the stages that have to be followed over the product lifetime.

.. figure::  img/project_timeline.png
   :align:   center

   Typical project timeline

While the development process is mainly handled using GITLab, which provide a nice interface to follow the version and work in team, the PSMS tool targets all of the other stages. The objective of it is to provide a common interface, versatile enough, to handle all kinds of projects.

PSMSFramework
=============

In the PSMS suite context, the PSMSFramework provides a generic infrastructure that ease the development of the tester. Indeed, after producing devices, all of them has to be tested – either manually or automatically. However, developing a tester is not only taking care of the low level functions but also the data storage and the graphical user interface. The Figure 2 below shows the typical tester architecture.

.. figure::  img/typical_tester_architecture.png
   :align:   center

   Typical tester architecture

According to the tester architecture block diagram, the GUI and database interface are common needs for the project. The GUI is responsible for providing a way to execute test and display them, via reports and the database interface aims to push the results into a database to store them for post-treatment analyses. Finally, only the user scripts are specific to the project and contains the low level functions to access the hardware and to execute the test procedures.