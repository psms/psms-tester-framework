# PSMSFramework package

The Product and Support Management System (PSMS) provides an interface to manage all of the common part that have to be handled during a product lifetime.
In this context, the PSMSTestFramework provides a complete framework to ease the development of automatic testers.
This documentation aims to describe the tool and its use.

## Installation:

python3 -m pip install git+https://gitlab.cern.ch/psms/psms-tester-framework.git
