''' Comment to use the package installed via pip '''
import sys
sys.path.insert(1, './../')
''' End of comment '''

import PSMSTestFramework

import time

if __name__ == "__main__":
    PSMSTestFramework.PSMSTestFramework(
        title='<b>TEMPLATE</b> Tester',

        useGUI=True,

        git_root='../',
        tests_root='tests/',
        pickle_root='results/',
        export_root='export/',
        runner_log_root='runlogs/',

        #Enable restAPI push
        http_push_url='https://cern-dms-template.web.cern.ch/',
        http_push_key='292d5c8593214408a801e98bc32a9791',

        #Enable CERN based login
        oauth_client_id='psms',
        oauth_token_url= 'https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token',
        oauth_auth_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth',
        oauth_api_url='https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/',

        #Enable update (to be added)
        #update_enable = True,

        #Enable local accounts
        local_accounts = [
            {
                'username': 'local',
                'password': 'local',
                'roles': ['viewer','runner','test','debug']
            }
        ],

        #Enable operator account
        operator_roles = ['viewer','runner','test','debug']

    )
