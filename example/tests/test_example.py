import random
import time
import numpy as np
import getpass

import PSMSTestFramework

import debug_example

@PSMSTestFramework.sysapi.register_test('Example [8 GPIOs tester]')
def gpio_ex_tester():
    """The '[Example GPIO] tester' check the GPIO feature.

    This test emulate a full test of a GPIO. In this case, each I/O is tested in a subtest instance
    in order to measure the maximum output current in output mode and the voltage thresholds in
    input. In addition, it checks the output voltage when the pin is set to low.

    .. graphviz::

        digraph G {
            node [fontname = "Handlee"];
            edge [fontname = "Handlee"];

            start [
                label = "Start";
                shape = oval;
            ];

            askDevice [
                label = "Ask for device ID";
                shape = rect;
            ];

            continueTestint [
                label = "I/O id < 8?";
                shape = diamond;
            ];

            runsubtest [
                label = "Run gpio_subtest(id)";
                shape = rect;
            ];

            isIOFalse [
                label = "Check subtest status";
                shape = diamond;
            ];

            testFails [
                label = "Set general test as failed";
                shape = rect;
            ];

            wait [
                label = "Set progress and wait 0.5s";
                shape = rect;
            ];

            end [
                label = "End";
                shape = oval;
            ];



            start -> askDevice;
            askDevice:s -> continueTestint:n;

            continueTestint:s -> runsubtest:n [label = "Yes"];
            continueTestint -> end [label = "No"];

            runsubtest -> isIOFalse
            isIOFalse -> testFails [label = "Fail"]
            testFails:s -> wait:e
            isIOFalse -> wait [label = "Pass"]
            wait:w -> continueTestint:w

            {
                rank=same;
                continueTestint; end;
            }
            {
                rank=same;
                isIOFalse; testFails;
            }
        }

    When finished, the test returns a dictionnary formated to be compliant with the
    psms-tester framework. The result is then **displayed in the user's browser** according
    to the display setting and **pushed into the database**.
    """

    #Get device details
    dev_sn = PSMSTestFramework.sysapi.get_data('Device BN/SN','''
        Please, inform what is the device under test. To do this you can eithe:
        <ul>
            <li>Scan the QR code located on the device sticker</li>
            <li>Enter the BN/SN value manually</li>
        </ul>
    ''')

    boardSplittedRef = dev_sn.rstrip().split("/")

    DEVICE = {
        'name': 'vldbplus_v0',
        'details': {
            'build_number': boardSplittedRef[0],
            'serial_number': boardSplittedRef[1],
        },
        'batch': {
            'name': 'FAKEModule',
            'details': {
                'Build number': boardSplittedRef[0]
            }
        }
    }

    #Run subtests
    SUBTESTS = []
    global_res = 0x00

    for i in range(0, 8):

        s = debug_example.gpio_subtest(
            io_id=i,
            input_min_voltage=0,
            input_max_voltage=1.2,
            input_steps=0.1,
            output_min_load=0,
            output_max_load=10,
            output_steps=0.1,
            high_voltage_threshold=0.8
        )

        PSMSTestFramework.sysapi.set_job_progress(10+(i*10))

        SUBTESTS.append(s)

        if s['pass'] != 0:
            global_res |= -s['pass']

        time.sleep(0.5)

    #get test details
    global_res = global_res * (-1)

    return {
        'pass': global_res,
        'details': {
            'operator': getpass.getuser()
        },
        'device': DEVICE,
        'subtests': SUBTESTS,

        'display': [
            {
                'test_type_name': 'GPIO_EMUL_v0',
                'summary': {
                    'type': 'table', #Only table is supported for subtest display
                    'columns': [
                        {'col': 'summary', 'key': 'min_high_detect', 'verbose': 'Min. input voltage detected'},
                        {'col': 'summary', 'key': 'max_zero_detect', 'verbose': 'Max. input voltage not detected'},
                        {'col': 'summary', 'key': 'min_high_set', 'verbose': 'Min. output load supported'},
                        {'col': 'summary', 'key': 'max_zero_set', 'verbose': 'Max. output load not supported'}
                    ]
                },
                'measurements': [
                    {
                        'title': 'Input voltage test',
                        'doc': 'During the input voltage test, the digital value is get for different voltages applied on the pin. It checks the high level threshold by going from a low to a high value.',
                        'type': 'table',
                        'point_names': 'input_voltage_detect',
                        'columns': [
                            {'key': 'v', 'verbose': 'Input voltage'},
                            {'key': 'r', 'verbose': 'Read back from device'},
                        ]
                    },{
                        'type': 'scatter',
                        'label': 'voltage vs. load',
                        'title': 'Output load test',
                        'doc': 'During the output load test, the output voltage is measured for different load values. It checks whether the output current is compliant with the specifications',
                        'point_names': 'output_load_test',
                        'xkey': 'l',
                        'ykey': 'v'
                    }
                ],
                'doc': 'Optional - you can explain here what does the test do. It will be used into the report :-)'
            }
        ]
    }
