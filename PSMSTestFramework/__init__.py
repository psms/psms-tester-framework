import signal
import os
import webview
import re
import git
import sys

from multiprocessing import Process

import PSMSTestFramework.WebServer as WebServer
import PSMSTestFramework.Worker as Worker
import PSMSTestFramework.ResPusher as ResPusher

def start_webview(title, fullscreen, port, mainpid):
    cleanr = re.compile('<.*?>')
    cleantitle = re.sub(cleanr, '', title)

    print('[Info] Open GUI')
    webview.create_window(cleantitle, 'http://localhost:{}/'.format(port),min_size=(1200, 800), fullscreen=fullscreen, confirm_close=False)

    try:
        webview.start()
    except:
        pass

    os.kill(mainpid, 9)

def signal_handler():
    raise Exception('Close')

class PSMSTestFramework:

    def __init__(self,
            title,
            git_root='.',
            tests_root='.',
            pickle_root=None,
            export_root='',
            port=8000,

            http_push_url=None,
            http_push_key=None,

            oauth_client_id=None,
            oauth_token_url= None,
            oauth_auth_url=None,
            oauth_api_url=None,
            local_accounts=[],
            operator_roles=[],
            useGUI=False,
            gui_fullscreen=False,
            runner_log_root=None
        ):

        #Direct connection not supported anymore
        db_host=None
        db_username=None
        db_password=None
        db_port=None
        db_dbname=None

        sys.path.insert(1, tests_root)

        if export_root and not os.path.exists(export_root):
            raise Exception('Exported root path "{}" does not exists'.format(export_root))

        if runner_log_root and not os.path.exists(runner_log_root):
            raise Exception('Runner log root path "{}" does not exists'.format(runner_log_root))

        if pickle_root is None:
            pickle_tmp = None
            pickle_success = None
            pickle_data_error = None
            pickle_http_error = None

        else:
            if not os.path.exists(pickle_root):
                raise Exception('Pickle root path "{}" does not exists'.format(pickle_root))

            pickle_tmp = os.path.join(pickle_root, 'to_be_sent')
            pickle_success = os.path.join(pickle_root, 'success')
            pickle_data_error = os.path.join(pickle_root, 'data_error')
            pickle_http_error = os.path.join(pickle_root, 'http_error')

            if pickle_tmp and not os.path.exists(pickle_tmp):
                os.mkdir(pickle_tmp)
            if pickle_success and not os.path.exists(pickle_success):
                os.mkdir(pickle_success)
            if pickle_data_error and not os.path.exists(pickle_data_error):
                os.mkdir(pickle_data_error)
            if pickle_http_error and not os.path.exists(pickle_http_error):
                os.mkdir(pickle_http_error)


        if db_host is None:
            print('[Info] Database interface is not set')
        else:
            if db_username is None:
                print('[Error] Database interface is missing db_username info')
            if db_password is None:
                print('[Error] Database interface is missing db_password info')
            if db_port is None:
                print('[Error] Database interface is missing db_port info')
            if db_dbname is None:
                print('[Error] Database interface is missing db_dbname info')

        try:
            repo = git.Repo(git_root)
            version = 'SHA.{}'.format(repo.commit())
        except Exception as e:
            version = 'Unknown vers.'

        print('[Info] Version {}'.format(version))

        #Start services
        cleanr = re.compile('<.*?>')
        cleantitle = re.sub(cleanr, '', title)

        if useGUI:
            p = Process(target=start_webview, args=(title,gui_fullscreen,port, os.getpid()))
            p.start()

        #signal.signal(signal.SIGINT, signal_handler)

        try:
            worker = Worker.Worker(
                        tester_name=cleantitle,
                        tester_version=version,
                        runner_log = runner_log_root,
                        pickle_tmp = pickle_tmp,

                        http_push_url=http_push_url,
                        http_push_key=http_push_key,
                    )

            webserver = WebServer.WebServer(
                title=title,
                worker=worker,
                git_root=git_root,
                tests_root=tests_root,
                export_root=export_root,
                port=port,
                oauth_client_id=oauth_client_id,
                oauth_token_url= oauth_token_url,
                oauth_auth_url=oauth_auth_url,
                oauth_api_url=oauth_api_url,
                local_accounts=local_accounts,
                operator_account=operator_roles,
                runnerlogs_root=runner_log_root
            )

            respusher = ResPusher.ResPusher(pickle_root = pickle_root, http_push_url = http_push_url, http_push_key = http_push_key)
            respusher.start()

            webserver.run()

        except KeyboardInterrupt:
            respusher.stop()
            respusher.join()

            if useGUI:
                os.kill(p.pid, 9)

        '''
            KILL PROCESS
        '''
        '''
        #os.killpg(os.getpgid(webServerProcess.pid), signal.SIGTERM)
        print('[Deb.] Webserver is alive: {}'.format(webServerProcess.is_alive()))
        os.kill(webServerProcess.pid, 9)
        webServerProcess.join()
        print('[Deb.] Webserver is alive: {}'.format(webServerProcess.is_alive()))
        '''