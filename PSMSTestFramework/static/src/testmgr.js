var testmgr = function(action_id, callback){

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    var root = this;
    var act_node = action_id;
    var run_cback = callback;

    this.showLoading = function(){
        $('#loading-node').fadeIn(200);
    }
    this.hideLoading = function(){
        $('#loading-node').fadeOut(200);
    }

    this.update_status = function(is_running, paused, stopped){
        if(is_running){
            $('#run-btn').html('<i class="fa fa-spinner fa-spin"></i><span class="w3-hide-small w3-margin-left">RUNNING ...</span>');
            $('#run-btn').prop('disabled', true);
            $('#action-selector').prop('disabled', true);


            $('#stop-btn').show();
            $('#pause-btn').show();
            $('#stop-btn').removeAttr("disabled");
            $('#pause-btn').removeAttr("disabled");

            if(paused){
                $('#pause-btn').html('<b><i class="fas fa-play w3-text-orange"></i><span class="w3-hide-small w3-margin-left">RESUME</span></b>');
                $('#pause-alert').slideDown(100);

            } else {
                $('#pause-btn').html('<b><i class="fas fa-pause w3-text-orange"></i><span class="w3-hide-small w3-margin-left">PAUSE</span></b>');
                $('#pause-alert').hide();

            }

            if(stopped){
                $('#stop-btn').prop('disabled', true);
                $('#stop-alert').slideDown(100);

            }else{
                $('#stop-alert').hide();

            }

        }else{
            $('#stop-alert').hide();
            $('#pause-alert').hide();

            $('#stop-btn').hide();
            $('#pause-btn').hide();

            $('#action-selector').removeAttr("disabled");
            $('#run-btn').removeAttr("disabled");
            $('#run-btn').html('<b><i class="w3-text-green fas fa-play"></i><span class="w3-hide-small w3-margin-left">RUN</span></b>');

        }
    }

    /*
     * Constructor
     */
    this.construct = function(){
        $('#'+act_node).html(
                '<div class="w3-row">'+
                '   <div class="w3-col l3 m5 s7 w3-container w3-margin-bottom">'+
                '    <select id="action-selector" class="w3-select">'+
                '        <option disabled selected>Select an action</option>'+
                '    </select>'+
                '   </div>'+
                '   <div class="w3-col l9 m7 s5 w3-container">'+
                '    <button id="run-btn" class="w3-button w3-border" disabled><b><i class="w3-text-green fas fa-play"></i><span class="w3-hide-small w3-margin-left">RUN</span></b></button>'+
                '    <button id="stop-btn" style="display: none" class="w3-button w3-border" disabled><b><i class="fas fa-stop w3-text-red"></i><span class="w3-hide-small w3-margin-left">STOP</span></b></button>'+
                '    <button id="pause-btn" style="display: none" class="w3-button w3-border" disabled><b><i class="fas fa-pause w3-text-orange"></i><span class="w3-hide-small w3-margin-left">PAUSE</span></b></button>'+
                '   </div>'+
                '</div><div class="w3-row">'+
                '   <div id="action-error"></div>'+
                '</div>'+
                '<div id="param-form" class="w3-row">'+
                '</div>'
            );

        $('body').append(''+
            '<div id="action-warning-modal" class="w3-modal">'+
            '  <div class="w3-modal-content">'+
            '    <div class="w3-container w3-padding">'+
            '        <span onclick="document.getElementById(\'action-warning-modal\').style.display=\'none\'"  class="w3-button w3-display-topright">&times;</span>'+
            '        <h4>Warnings</h4>'+
            '        <div class="w3-row">'+
            '            <div id="action-warning" class="w3-orange w3-container w3-card" style="text-align: justify">'+
            '            </div>'+
            '        </div>'+
            '    </div>'+
            '  </div>'+
            '</div>');

        $('body').append(''+
            '<div id="loading-node" style="'+
               ' z-index: 3;'+
               ' position: fixed;'+
               ' left: 0;'+
               ' top: 0;'+
               ' width: 100%;'+
               ' height: 100%;'+
               ' overflow: auto;'+
               ' background-color: rgb(0,0,0);'+
               ' background-color: rgba(0,0,0,0.4);'+
            '"><span style="'+
               ' font-size: 30px;'+
               ' top: 50%;'+
               ' left: 50%;'+
               ' -ms-transform: translate(-50%, -50%);'+
               ' transform: translate(-50%, -50%);'+
               ' position: absolute;'+
               ' color: #fff;'+
               ' text-shadow: 2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000;'+
            '"><i class="fa fa-spinner fa-spin"></i> Loading...</span></div>');

        $('#run-btn').click(function(){

            var function_id = $('#action-selector option:selected').val();
            var params = {};

            $('#param-form').find('input').each(function(){
                params[$(this).attr('name')] = $(this).val();
            });

            run_cback(function_id, params);
        });

        $('#stop-btn').click(function(){
            $('#stop-btn').prop('disabled', true);

            fetch('/stop', {
                method: 'GET',
                credentials: 'include'
            })

            .then( (response) => {

                if(!response.ok){
                    printError('<b>Server error:</b> HTTP '+response.status);
                    return null;
                }
                return response.json();
            })

            .then( (json) => {
                if(json['state']){
                    $('#stop-btn').prop('disabled', true);
                    $('#stop-alert').slideDown(100);

                }else{
                    $('#stop-alert').hide();

                }
            })
        });

        $('#pause-btn').click(function(){
            //$('#pause-btn').prop('disabled', true);

            fetch('/pause', {
                method: 'GET',
                credentials: 'include'
            })

            .then( (response) => {

                if(!response.ok){
                    printError('<b>Server error:</b> HTTP '+response.status);
                    return null;
                }
                return response.json();
            })

            .then( (json) => {
                if(json['state']){
                    $('#pause-btn').html('<b><i class="fas fa-play w3-text-orange"></i><span class="w3-hide-small w3-margin-left">RESUME</span></b>');
                    $('#pause-alert').slideDown(100);

                } else {
                    $('#pause-btn').html('<b><i class="fas fa-pause w3-text-orange"></i><span class="w3-hide-small w3-margin-left">PAUSE</span></b>');
                    $('#pause-alert').hide();

                }
            })
        });

        $('#action-selector').change(function(){
            $('#run-btn').removeAttr("disabled");
            $('#run-btn').html('<b><i class="w3-text-green fas fa-play"></i><span class="w3-hide-small w3-margin-left">RUN</span></b>');

            $('#param-form').html('');

            var pkg = $('#action-selector option:selected').attr('data-package');
            var fn = $('#action-selector option:selected').attr('data-function');

            fetch('/get_param_list', {
                method: 'POST',
                credentials: 'include',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    id: $(this).val()
                }),
            })

            .then( (response) => {
                if(!response.ok){
                    printError('<b>Server error:</b> HTTP '+response.status);
                    return null;
                }
                return response.json()
            })

            .then( (json) => {
                if(json == null) return;

                if (json['params'].length == 0){
                    return;
                }

                var form = '';

                $.each( json['params'], function( index, value ){
                    form += '<div class="w3-col l12 w3-container w3-margin-bottom">'+
                            '   <label><b>'+value+'</b></label>'+
                            '   <input type="text" name="'+value+'" class="w3-input w3-border-bottom"/>'+
                            '</div>';
                });

                $('#param-form').html(form);
            })
        });
    };

    this.update = function(){

        this.showLoading();

        fetch('/list', {
            method: 'GET',
            credentials: 'include'
        })

        .then( (response) => {
            this.hideLoading();

            if(!response.ok){
                printError('<b>Server error:</b> HTTP '+response.status);
                return null;
            }
            return response.json()
        })

        .then( (json) => {

            $('#action-warning').html('');

            $.each(json['errors'], function(key, value){
                printWarning('<span>In package '+value['package']+'</span> <pre style="margin-left:16px"><code>'+value['traceback']+'</code></pre>');
            });

            var options = '<option disabled selected>Select an action</option>';

            if(json['runners'].length > 0){
                options += '<optgroup label="Runners">'

                $.each(json['runners'], function(key, value){
                    options += '<option value="'+value['id']+'">'+value['name']+'</option>'
                });

                options += '</optgroup>'
            }

            if(json['tests'].length > 0){
                options += '<optgroup label="Tests">'

                $.each(json['tests'], function(key, value){
                    options += '<option value="'+value['id']+'">'+value['name']+'</option>'
                });

                options += '</optgroup>'
            }


            if(json['debug'].length > 0){
                options += '<optgroup label="Debug">'

                $.each(json['debug'], function(key, value){
                    options += '<option value="'+value['id']+'">'+value['name']+'</option>'
                });

                options += '</optgroup>'
            }

            $('#action-selector').html(options)
            if(json['errors'].length > 0){
                $('#warning-count').html(''+json['errors'].length)
                $('#warning-btn').show();
            }
        })
    };

    var printError = function(msg){
        $('#action-error').append('<div class="w3-red w3-container w3-card" style=""><b>Error:</b> '+msg+'</div>');
    }

    var printWarning = function(msg){
        $('#action-warning').append('<div style=""><b>Warning:</b> '+msg+'</div>');
    }

    this.construct();

};