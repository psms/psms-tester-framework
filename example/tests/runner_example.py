import PSMSTestFramework
import random
import getpass
import time

def stop_cback():
    print('[Info] Stop callback has been called')

@PSMSTestFramework.sysapi.register_runner('Example [8 GPIOs tester - prod.]', init_bins=['bin0','bin1','bin2'])
def runner_example():
    """The 'Test function Name' emulates an interface test.

    The 'Test function Name' check 5 interfaces using the 'Debug function Name' subtest.
    The flowchart below describe in details the test procedure.

    .. graphviz::

        digraph G {
            start [
                label = "Start";
                shape = oval;
            ];

            continueTestint [
                label = "Intf. id < 5?";
                shape = diamond;
            ];

            runsubtest [
                label = "Run function_dbg(id, 0, 100)";
                shape = rect;
            ];

            isIOFalse [
                label = "Check subtest status";
                shape = diamond;
            ];

            testFails [
                label = "Set general test as failed";
                shape = rect;
            ];

            wait [
                label = "Wait 0.1s";
                shape = rect;
            ];

            end [
                label = "End";
                shape = oval;
            ];



            start -> continueTestint;

            continueTestint:s -> runsubtest:n [label = "Yes"];
            continueTestint -> end [label = "No"];

            runsubtest -> isIOFalse
            isIOFalse -> testFails [label = "Fail"]
            testFails:s -> wait:e
            isIOFalse -> wait [label = "Pass"]
            wait:w -> continueTestint:w

            {
                rank=same;
                continueTestint; end;
            }
            {
                rank=same;
                isIOFalse; testFails;
            }
        }

    When finished, the test returns a dictionnary formated to be compliant with the
    psms-tester framework. Then, the result is **displayed in the user's browser** according
    to the display settings and **pushed into the database**.
    """

    #Print a message in the console
    print('[info] Debug function called')

    if PSMSTestFramework.sysapi.is_initialized() == False:
        print('[info] Not initialized yet!')
        PSMSTestFramework.sysapi.get_yesno("Ready?",'''Is it well initialized?''')

    res = PSMSTestFramework.sysapi.get_multi_data(
        [
            {'name': 'Operator name', 'Desc': 'Name of the operator'},
            {'name': 'Serial number', 'Desc': 'Device serial number'}
        ]
    )

    print(res)

    intfId = 0
    minvalue = 0
    maxvalue = 100

    #Measurement points variable
    meas = []
    randArr = []

    #Emulate a measurement
    for i in range(100):
        v = random.randint(int(minvalue),int(maxvalue))
        randArr.append(v)

        #Print the random value
        print('[meas] Random value ({}): {}'.format(i, v))

        #Store formatted measurement
        meas.append({
                'name': 'random_val',
                'data': {'v': v},
                'pass': 0
            })

        PSMSTestFramework.sysapi.set_job_progress(i)
        time.sleep(0.01)

    #Emulate pass/fail
    foo = [0, 0, 0, 0, 0, 0, 0, -0x01, -0x02, 0x01, 0x01]

    passStatus = random.choice(foo)
    if passStatus == 0:
        bin = 'bin0'
    elif passStatus < 0:
        bin = 'bin1'
    else:
        bin = 'bin2'

    verboseDetails = []
    if passStatus == -1:
        verboseDetails.append('GPIO Error')
    if passStatus == -2:
        verboseDetails.append('I2C Error')
    if passStatus == 2:
        verboseDetails.append('I2C Warning')
    if passStatus == 1:
        verboseDetails.append('GPIO Warning')

    #Check stop to emulate a stop procedure
    if PSMSTestFramework.sysapi.is_stopped():
        print('[STOP] Stop button has been clicked')

    #Returns the subtest result
    return {
        'pass': passStatus,
        'bin': bin, #optional

        'details': {
            'operator': 'unknown' #PSMSTestFramework.sysapi.operator_id()
        },

        'device': {
            'name': 'vldbplus_v0',
            'details': {
                'Build number': '0000 0000',
                'Serial number': '0000 0000',
            },
            'batch': {
                'name': 'FAKEModule',
                'details': {
                    'Build number': '0000 0000'
                }
            }
        },

        'subtests': [
            {
                'test_type_name': 'FN_DBG_EX',
                'name': 'fn_dbg_ex_{}'.format(intfId),

                'details': {
                    'intfId': intfId,
                    'minvalue': minvalue,
                    'maxvalue': maxvalue
                },

                'summary': {
                    'avg': (sum(randArr)/len(randArr)),
                    'max': max(randArr),
                    'min': min(randArr)
                },

                'measurements': meas,
                'pass': 0
            }
        ],

        'verboseDetails': verboseDetails
    }
