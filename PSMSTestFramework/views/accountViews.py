import json
import base64
import re

from flask import redirect
from flask import request
from flask import session

from authlib.integrations.flask_client import OAuth

import PSMSTestFramework.sysapi as sysapi

class AccountViews():
    '''
    test Views.
    '''

    def __init__(
            self,
            application,

            oauth_client_id=None,
            oauth_token_url= None,
            oauth_auth_url=None,
            oauth_api_url=None,
            local_accounts=[],
            operator_account=[]
        ):

        self.application = application

        self.application.add_url_rule('/disconnect', 'disconnect', self.disconnect,methods=['GET'])

        if len(operator_account) == 0:
            self.operator_account = None

        else:
            self.operator_account = operator_account
            self.application.add_url_rule('/operator_connected', 'operator_login_callback', self.operator_login_callback,methods=['POST'])

        if len(local_accounts) == 0:
            self.local_client = None

        else:
            self.local_client = local_accounts
            self.application.add_url_rule('/local_connected', 'local_login_callback', self.local_login_callback,methods=['POST'])

        if oauth_client_id is not None:
            oauth = OAuth(self.application)
            self.oauth_client = oauth.register(
                name='cern-dms',
                client_id=oauth_client_id,
                access_token_url=oauth_token_url,
                access_token_method='POST',
                authorize_url=oauth_auth_url,
                api_base_url=oauth_api_url
            )

            self.application.add_url_rule('/oauth_connected', 'oauth_login_callback', self.oauth_login_callback,methods=['GET','POST'])
            self.application.add_url_rule('/oauth_connect', 'oauth_connect', self.oauth_connect)

        else:
            self.oauth_client = None

        self.oauth_client_id = oauth_client_id

    def is_signin_needed(self):
        login = False
        oauth_loggin = False
        local_loggin = False
        operator_loggin = False

        if self.oauth_client is not None:
            oauth_loggin = True
            login = True

        if self.local_client is not None:
            local_loggin = True
            login = True

        if self.operator_account is not None:
            operator_loggin = True
            login = True

        if login == False:
            session['roles'] = ['debug','viewer','runner','test']
            session['username'] = 'Unknown'

            return False

        if 'username' not in session:
            return True

        return False

    def supported_signin(self):
        return {
            'oauth': True if self.oauth_client is not None else False,
            'local': True if self.local_client is not None else False,
            'operator': True if self.operator_account is not None else False,
        }

    def is_connected(self):
        if 'username' not in session:
            return False

        return True

    def get_current_roles(self):
        if 'roles' not in session:
            return []

        return session['roles']

    def disconnect(self):
        session.pop('username')
        session.pop('roles')

        return redirect('/')

    def operator_login_callback(self):
        session['username'] = request.form['operator_id']
        session['roles'] = self.operator_account

        return redirect('/')

    def local_login_callback(self):
        username = request.form['username']
        password = request.form['password']

        for i in self.local_client:
            if username == i['username'] and password == i['password']:
                session['username'] = username
                session['roles'] = i['roles']

                return redirect('/')

        login = False
        oauth_loggin = False
        local_loggin = False
        operator_loggin = False

        if self.oauth_client is not None:
            oauth_loggin = True
            login = True

        if self.local_client is not None:
            local_loggin = True
            login = True

        if self.operator_account is not None:
            operator_loggin = True
            login = True

        cleanr = re.compile('<.*?>')
        cleantitle = re.sub(cleanr, '', self.title)

        return render_template('loggin.html', title=self.title, cleantitle=cleantitle, oauth_loggin=oauth_loggin, local_loggin=local_loggin, local_connection_error=True, operator_loggin=operator_loggin)

    def oauth_connect(self):
        if self.oauth_client is not None:
            return self.oauth_client.authorize_redirect('{}oauth_connected'.format(request.url_root))

    def oauth_login_callback(self):
        if self.oauth_client is not None:
            try:
                token = self.oauth_client.authorize_access_token()

                token_split = token['access_token'].split('.')

                for i in range(4-(len(token_split[1])%4)):
                    token_split[1] += '='

                roles = json.loads(base64.b64decode(token_split[1]))['resource_access'][self.oauth_client_id]['roles']
                details = self.oauth_client.get('userinfo').json()

                session['username'] = details['cern_upn']
                session['roles'] = roles

                return redirect('/')

            except Exception as exception:
                print('Exception: {}'.format(exception))
                return 'Internal error: please check that you accept the cookies and retry. Only keycloak authentication is supported'