PSMSFramework
*************

The Product and Support Management System (PSMS) provides an interface to manage all of the common part that have to be handled during a product lifetime. In this context, the PSMSFramework provides a complete framework to ease the development of automatic testers. This documentation aims to describe the tool and its use.

.. toctree::
   :maxdepth: 2
   :caption: User guide

   Introduction <introduction>
   Install <how_to_start>
   Debug functions <add_debug>
   Test functions <add_test>
   Test output formatting <test_output>
   Interractive functions <interractive_functions>
   Advanced options <advanced_options>

.. toctree::
   :maxdepth: 2
   :caption: API source code

   Webserver <webserver>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`