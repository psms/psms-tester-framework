Install
*******

PSMSFramework is a python package that can be easily installed using the common package installer for python (PIP). Because at the first development stage, the python version is 3.8 and some functions added in python 3.4 are used in the library, it requires working with version higher to 3.4. The following instruction will install the package:

.. code-block:: bash

    python3 –m pip install git+https://gitlab.cern.ch/jumendez/psms-framework.git

Once it is successfully installed on your machine, you can create a new project based on this package as illustrated below:

.. code-block:: python

    import PSMSFramework

    if __name__ == "__main__":
        PSMSFramework.PSMSFramework(
                title='<b>TEMPLATE</b> Tester'
            )

Finally, the script has to be executed using the command line interface:

.. code-block:: bash

    python3 main.py

The PSMSFramework class will automatically list all of the test located into the project and start the webserver.

Finally, the tester can be accessed using a web browser via the following address:

* http://IPAddress:8000 (e.g.: http://127.0.0.1:8000).

Then, you end-up with the following interface:

.. figure::  img/init_browser.png
   :align:   center

   Default GUI