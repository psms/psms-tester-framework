Test output formatting
**********************

The DMSTestFramework library generates automatically reports and pushes the result to the DMSDatabase. Therefore, a specific formatting is required.
This section describe the dictionnary to be returned.

.. code-block:: python

    return {
        'pass': passStatus,
        'passDetails': {},
        'bin': binName,
        'details': {},
        'device': {},
        'subtests': [],
        'display': []
    }

Each key contains details about the test that was carried out:

Pass result
===========

* pass: <32b integer value> reprensiting the test status, where:
    * 0: Success
    * <0: Failure
    * >0: Warning

For the `pass` value, it is recommended to follow a philosophie where each bit represent a type of subtest. As an example, we will consider a 4ch power supply tester. Therefore, to validate the device, we need to run the following types of subtests:

* Output voltage accuracy: Measurement of the voltage delta between the command and the actual output whithout any load connected. This type of subtest is ran 4 times, once for each channel.
* Output efficiency: Measurement of the output efficiency of each channel (output power vs. input power).
* Load regulation: Measurement of output voltage for different load levels. This test is performed for each channel.

Therefore, we could imagine that the pass variable is computed as following:

* Bit 0: '1' if one of the 4 `Output voltage accuracy` test had an issue (Error or warning)
* Bit 1: '1' if one of the 4 `Output voltage accuracy` test had an issue (Error or warning)
* Bit 2: '1' if one of the 4 `Output voltage accuracy` test had an issue (Error or warning)

And finally, the value is inversed when the test failed, or kept positive otherwise.

.. note:
    The example above can be modified! For example, if less than 32 failure/warning types are known, each bit can correspond to one failure type instead of one subtest. Additionally, if only one failure has to be stored (e.g.: the most critical one), a raw value can be stored instead of a bit mask. The only restriction is to keep it positive or negative depending on warning/success/error.

Pass details
============

* passDetails: <array of dictionnary> containing the pass variable description. It allows to make the result verbose.

The `passDetails` dictionnary describe how to translate the pass variable to a verbose