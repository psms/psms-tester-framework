import threading
import sys
import os
import json
import subprocess
import importlib
import re
import traceback
import socket
import base64
import time

from flask import redirect
from flask import Flask
from flask import render_template
from flask import request
from flask import send_file
from flask import session

from gevent import pywsgi
from gevent.pool import Pool

import PSMSTestFramework.sysapi as sysapi
import PSMSTestFramework.views.testViews as testViews
import PSMSTestFramework.views.accountViews as accountViews
import PSMSTestFramework.views.updateViews as updateViews
import PSMSTestFramework.views.runnerViews as runnerViews

class WebServer():
    '''
    PSMSTestFramework webserver class runs the flask application.

    The init function initialize the Flask application and accept the following parameters:

    Args:
        title: Set the web application title
        worker: link to the test/debug function runner class
        git_root: set the root path of the git repository. Default = current folder
        test_root: set the root path of the test scripts. Default = curent folder

    The Flask server declares the following routes:

    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | Route           | Method     | Function                                                                    | Description                                                                |
    +=================+============+=============================================================================+============================================================================+
    | /               | GET        | `index <#PSMSTestFramework.WebServer.WebServer.index>`_                         | returns the main html page                                                 |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /version        | GET        | `get_version <#PSMSTestFramework.WebServer.WebServer.get_version>`_             | returns the local and server commit ids allowing to know whether an update |
    |                 |            |                                                                             | is available [status error when the project is not a git repo.]            |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /update         | GET        | `update_version <#PSMSTestFramework.WebServer.WebServer.update_version>`_       | triggers an update of the tester using git commands                        |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /list           | GET        | `get_function_list <#PSMSTestFramework.WebServer.WebServer.get_function_list>`_ | list the debug and test functions                                          |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /get_param_list | POST       | `get_param_list <#PSMSTestFramework.WebServer.WebServer.get_param_list>`_       | list all of the input parameters of a function                             |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /run            | POST       | `runfn <#PSMSTestFramework.WebServer.WebServer.runfn>`_                         | runs a specific debug or test function                                     |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /status         | GET        | `status <#PSMSTestFramework.WebServer.WebServer.status>`_                       | returns the current execution status                                       |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stdout         | GET        | `stdout <#PSMSTestFramework.WebServer.WebServer.stdout>`_                       | returns the stdout output                                                  |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stdin          | GET        | `get_stdin_req <#PSMSTestFramework.WebServer.WebServer.get_stdin_req>`_         | returns whether an input is required or not                                |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stdin          | POST       | `send_stdin <#PSMSTestFramework.WebServer.WebServer.send_stdin>`_               | sends user data to server                                                  |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /stop           | GET        | `stop_running <#PSMSTestFramework.WebServer.WebServer.stop_running>`_           | stop on-going action                                                       |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    | /files          | GET        | `list_files <#PSMSTestFramework.WebServer.WebServer.list_files>`_               | List exported files                                                        |
    +-----------------+------------+-----------------------------------------------------------------------------+----------------------------------------------------------------------------+

    The template and static folders are linked to the package **/template** and **/static** folders.
    Finally, it also implements a `run <#PSMSTestFramework.WebServer.WebServer.run>`_  and a `kill <#PSMSTestFramework.WebServer.WebServer.kill>`_ method allowing to start/stop the server from the main thread.

    **Methods description:**

    The chapters below details all of the methods implemented in the WebServer class.

    '''

    def __init__(   self,
                    title,
                    worker,
                    git_root='.',
                    tests_root='.',
                    export_root='',
                    runnerlogs_root=None,
                    port=8000,
                    oauth_client_id=None,
                    oauth_token_url= None,
                    oauth_auth_url=None,
                    oauth_api_url=None,
                    local_accounts=[],
                    operator_account=[]
        ):

        script_route = os.path.abspath(os.path.dirname(__file__))
        template_dir = os.path.join(script_route, 'templates')
        static_dir = os.path.join(script_route, 'static')

        self.application = Flask(__name__, template_folder=template_dir, static_folder=static_dir)
        self.application.config['TEMPLATES_AUTO_RELOAD'] = True
        self.application.secret_key = 'G8dsr58huffffgdsdgj002'

        print('HERE WE ARE')

        self.testViewsInst = testViews.TestViews(self.application, tests_root)
        self.accountViewsInst = accountViews.AccountViews(self.application, oauth_client_id, oauth_token_url, oauth_auth_url, oauth_api_url, local_accounts, operator_account)
        self.updateViewsInst = updateViews.UpdateViews(self.application, git_root)
        self.runnerViewsInst = runnerViews.RunnerViews(self.application, self.testViewsInst.get_function_list, worker)

        self.application.add_url_rule('/', 'index', self.index, methods=['GET'])
        #self.application.add_url_rule('/status', 'status', self.status, methods=['GET'])
        #self.application.add_url_rule('/stdout', 'stdout', self.stdout, methods=['GET'])
        #self.application.add_url_rule('/stdin', 'get_stdin_req', self.get_stdin_req, methods=['GET'])
        #self.application.add_url_rule('/stdin', 'send_stdin', self.send_stdin, methods=['POST'])
        #self.application.add_url_rule('/stop', 'stop_running', self.stop_running, methods=['GET'])
        #self.application.add_url_rule('/pause', 'pause_running', self.pause_running, methods=['GET'])
        self.application.add_url_rule('/files', 'list_files', self.list_files, methods=['GET'])
        self.application.add_url_rule('/file/<filename>', 'get_file', self.get_file, methods=['GET'])
        self.application.add_url_rule('/logs', 'list_logs', self.list_logs, methods=['GET'])
        self.application.add_url_rule('/log/<filename>', 'get_log', self.get_log, methods=['GET'])

        self.title = title
        self.port = port

        self.tests_root = tests_root
        self.export_root = os.path.join(os.getcwd(), export_root)
        self.runnerlogs_root = os.path.join(os.getcwd(), runnerlogs_root)

        self.worker = worker

        super().__init__()

    def stop_running(self):
        #if 'test' in session['roles'] or 'debug' in session['roles'] or 'runner' in session['roles']:
        #    return json.dumps({'status': self.worker.stop()})
        #else:
        #    return json.dumps({'status': -2})
        return json.dumps({'status': -2})

    def pause_running(self):
        #if 'runner' in session['roles']:
        #    return json.dumps({'status': self.worker.pause_fn()})
        #else:
        #    return json.dumps({'status': -2})
        return json.dumps({'status': -2})

    def get_file(self, filename):
        if not os.path.exists(os.path.join(self.export_root, filename)) or not os.path.isfile(os.path.join(self.export_root, filename)):
            abort(404)

        return send_file(os.path.join(self.export_root, filename))

    def get_log(self, filename):
        if not os.path.exists(os.path.join(self.runnerlogs_root, filename)) or not os.path.isfile(os.path.join(self.runnerlogs_root, filename)):
            abort(404)

        return send_file(os.path.join(self.runnerlogs_root, filename))

    def list_files(self):
        if self.export_root == '':
            return json.dumps({'status': -1, 'msg': 'Exported root path is not set'})

        if not os.path.exists(self.export_root):
            return json.dumps({'status': -2, 'msg': 'Exported root path does not exist'})

        onlyfiles = [f for f in os.listdir(self.export_root) if os.path.isfile(os.path.join(self.export_root, f))]

        return json.dumps({'status': 0, 'files': onlyfiles})

    def list_logs(self):
        if self.runnerlogs_root is None:
            return json.dumps({'status': -1, 'msg': 'Log root path is not set'})

        if not os.path.exists(self.runnerlogs_root):
            return json.dumps({'status': -2, 'msg': 'Log root path does not exist'})

        onlyfiles = [f for f in os.listdir(self.runnerlogs_root) if os.path.isfile(os.path.join(self.runnerlogs_root, f))]

        return json.dumps({'status': 0, 'files': onlyfiles})

    def index(self):
        '''
        The index route returns the index.html template with the title set as an input parameters

        Returns:
            .. code-block:: python

                render_template(
                        'index.html',
                        title=self.title,
                        cleantitle=cleantitle
                    )
        '''
        cleanr = re.compile('<.*?>')
        cleantitle = re.sub(cleanr, '', self.title)

        if self.export_root != '':
            exportedFiles = True
        else:
            exportedFiles = False

        if self.runnerlogs_root is not None:
            runnerLogs = True
        else:
            runnerLogs = False

        if self.accountViewsInst.is_signin_needed():
            supported_signin = self.accountViewsInst.supported_signin()
            return render_template(
                'loggin.html',
                title=self.title,
                cleantitle=cleantitle,
                oauth_loggin=supported_signin['oauth'],
                local_loggin=supported_signin['local'],
                operator_loggin=supported_signin['operator']
            )

        return render_template(
            'index.html',
            title = self.title,
            cleantitle = cleantitle,
            exportedFiles = exportedFiles,
            connected = self.accountViewsInst.is_connected(),
            roles = self.accountViewsInst.get_current_roles(),
            runnerLogs = runnerLogs
        )

    def kill(self):
        '''
        The kill function allows stopping the webserver from the main threading
        '''
        print('[Info] Stop webserver...')
        self.server.stop()

    def status(self):
        '''
        Called on '/status' route:

        The status function returns the current test/debug function runner status.

        Returns:
            .. code-block:: javascript

                /** When idle - no function was called */
                {
                    'status': -2,
                    'msg': 'No function called'
                }

                /** Wait for running */
                {
                    'status': 2,
                    'fn': function_name,
                    'p': 0,
                    'type': type,
                        // '0': test
                        // '1': debug
                        // '2': runner
                    'name': name
                        // Test/debug function verbose name
                }

                /** Is running */
                {
                    'status': 1,
                    'fn': function_name,
                    'p': progress,
                        // current progress value
                    'type': type,
                        // '0': test
                        // '1': debug
                    'name': name
                        // Test/debug function verbose name
                }

                /** Successfully finished */
                {
                    'status': 0,
                    'fn': function_name,
                    'ret': ret,
                        // Dict. returned by the function
                    'p': 100,
                    'type': type,
                        // '0': test
                        // '1': debug
                    'doc': function_doc,
                        // from file or function docstring
                    'name': name
                        // Test/debug function verbose name
                }

                /** Finished with exception */
                {
                    'status': -1,
                    'fn': self.fn.__name__,
                    'error': str(e),
                        // Error title
                    'traceback': err,
                        // Traceback details
                    'p': 100,
                    'type': type,
                        // '0': test
                        // '1': debug
                    'doc': function_doc,
                        // from file or function docstring
                    'name': name
                        // Test/debug function verbose name
                }
        '''
        return json.dumps(self.worker.status())

    def stdout(self):
        '''
        The stdout function get the current stdout value from the function runner.

        Returns:
            .. code-block:: javascript

                {
                    'status': 0,
                    'msg' stdout
                }
        '''
        return json.dumps({'status': 0, 'data': self.worker.get_stdout()})

    def get_stdin_req(self):
        '''
        The get_stdin_req function checks whether the runner wait for a user input.

        Returns:
            .. code-block:: javascript

                /** No user input requested */
                {
                    'status': -1
                }

                /** Waits for an input */
                {
                    'status': 0,
                    'name': param_name,
                    'doc': description
                }
        '''
        return json.dumps(self.worker.is_waiting_for_stdin())

    def send_stdin(self):
        '''
        The send_stdin receives the user input from the http request body and pass it to the runner.

        Args:
            JSON:

                .. code-block:: javascript

                    {
                        'stdin': user_input
                    }

        Returns:
            .. code-block:: javascript

                /** No user input requested */
                {
                    'status': 0
                }
        '''
        data = request.json
        try:
            self.worker.set_stdin(data['stdin'])
        except Exception as e:
            return json.dumps({'status': -1, 'msg': str(e)})

        return json.dumps({'status': 0})

    def run(self):
        '''
        The run function starts the Flask application using the gevent WSGIServer. It spawn it over 5 workers to
        make the server more responsive.
        '''
        try:
            self.err = None

            print('[Info] Start webserver...')
            hostname = socket.gethostname()
            IPAddr = socket.gethostbyname(hostname)

            print("[Info] URL: http://{}:{}".format(hostname, self.port))

            self.server = pywsgi.WSGIServer(('0.0.0.0', self.port), self.application, spawn=5 and Pool(5) or 'default', log=None)
            self.server.serve_forever()

        except Exception as e:
            print('EXCEPTION: {}'.format(e))
            self.err = str(e)