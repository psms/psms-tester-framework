import json

from flask import redirect
from flask import request
from flask import session

import PSMSTestFramework.sysapi as sysapi

class TestViews():
    '''
    test Views.
    '''

    def __init__(
            self,
            application,
            tests_root
        ):

        self.application = application
        self.tests_root = tests_root

        self.application.add_url_rule('/list', 'update_function_list', self.update_function_list, methods=['GET'])
        self.application.add_url_rule('/get_param_list', 'get_param_list', self.get_param_list, methods=['POST'])

        self.function_list = None

    def get_function_list(self):
        return self.function_list

    def get_param_list(self):
        try:
            r = request.json
        except:
            return json.dumps({'status': -1})

        if 'id' not in r:
            return json.dumps({'status': -2})

        if self.function_list is None or 'functions' not in self.function_list:
            return json.dumps({'status': -3})

        try:
            for f in self.function_list['functions']:
                if int(f['id']) == int(r['id']):
                    return json.dumps({'status': 0, 'params': f['params']})

        except:
            return json.dumps({'status': -5})

        return json.dumps({'status': -4})

    def update_function_list(self):
        '''
        Import all of the files located into the test_root folder and subfolders to get all of the debug and test function registered.

        Returns:
            .. code-block:: javascript

                {
                    'tests': [
                            {
                                'id': function_id,
                                'params': function_params,
                                'package': test_fn_pkg,
                                'name': test_fn_verbose_name,
                                'function': test_fn_name
                            }, ...
                        ]
                    'debug': [
                            {
                                'id': function_id,
                                'params': function_params,
                                'package': dbg_fn_pkg,
                                'name': dbg_fn_verbose_name,
                                'function': dbg_fn_name
                            }, ...
                        ],
                    'errors': [
                            {
                                'package': package_name,
                                'error': title,
                                'traceback': traceback
                            }, ...
                        ]
                }

        '''

        if 'roles' not in session:
            return redirect('/index')

        self.function_list = sysapi.get_tester_functions(self.tests_root);
        functions = {'tests': [], 'debug': [], 'runners': [], 'errors': self.function_list['errors']}

        if 'test' in session['roles']:
            for f in self.function_list['functions']:
                if f['type'] == sysapi.TEST_FUNCTION:
                    functions['tests'].append({
                        'id': f['id'],
                        'package': f['package'],
                        'name': f['name'],
                        'function': f['function'].__name__,
                        'params': f['params']
                    })

        if 'debug' in session['roles']:
            for f in self.function_list['functions']:
                if f['type'] == sysapi.DEBUG_FUNCTION:
                    functions['debug'].append({
                        'id': f['id'],
                        'package': f['package'],
                        'name': f['name'],
                        'function': f['function'].__name__,
                        'params': f['params']
                    })

        if 'runner' in session['roles']:
            for f in self.function_list['functions']:
                if f['type'] == sysapi.RUNNER_FUNCTION:
                    functions['runners'].append({
                        'id': f['id'],
                        'package': f['package'],
                        'name': f['name'],
                        'function': f['function'].__name__,
                        'params': f['params']
                    })

        return json.dumps(functions)